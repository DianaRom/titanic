import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titatic_dataframe()

    '''
    Put here a code for filling missing values in the Titanic dataset for the 'Age' column and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df['Title'] = df['Name'].str.extract(r' ([A-Za-z]+)\.')

    def fill_age_by_title(group):
        median_age = group['Age'].median()
        return group['Age'].fillna(median_age)

    df['Age'] = df.groupby('Title')['Age'].transform(fill_age_by_title)

    median_ages = df.groupby('Title')['Age'].median().round().astype(int)
    missing_values = df.groupby('Title')['Age'].apply(lambda x: x.isnull().sum())

    result = [('Mr.', missing_values.get('Mr', 0), median_ages.get('Mr', None)),
              ('Mrs.', missing_values.get('Mrs', 0), median_ages.get('Mrs', None)),
              ('Miss.', missing_values.get('Miss', 0), median_ages.get('Miss', None))]

    return result

filled_values = get_filled()
print(filled_values)
